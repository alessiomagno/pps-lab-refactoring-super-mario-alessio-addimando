package GameComponent.Character;

import java.awt.*;

/**
 * Created by Alessio Addimando on 04/03/2017.
 */
public interface Character {

    Image walk(String nome , int frequenza);
}
