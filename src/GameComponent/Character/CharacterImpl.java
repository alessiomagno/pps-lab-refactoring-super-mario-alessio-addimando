package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

import GameComponent.Object.Element;
import GameLogic.Start;

public class CharacterImpl implements Character{

	private int l , h ; // dimensioni largezza e altezza
	private int x ,y ; // posizione iniziale
	protected boolean in_movimento ; // vero quando il personnagio camina
	protected boolean verso_destra ; // vero quando camina verso la destro false verso la sinistra
	public int contatore ; // frequenza passi effetuati
	protected boolean alive;


	public CharacterImpl(int X , int Y , int L , int H){

		this.x = X ;
		this.y = Y ;
		this.h = H ;
		this.l = L ;
		this.contatore = 0;
		this.in_movimento = false ;
		this.verso_destra = true ;
		this.alive = true;
	}


	// getters

	public boolean isAlive() {
		return alive;
	}

	public int getL() {
		return l;
	}
	public int getH() {
		return h;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public boolean isMooving() {
		return in_movimento;
	}
	public boolean isMoovingToRight() {
		return verso_destra;
	}
	public int getCounter() {
		return contatore;
	}

	// setters

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}
	public void setMooving(boolean in_movimento) {
		this.in_movimento = in_movimento;
	}
	public void setMoovingRight(boolean verso_destra) {
		this.verso_destra = verso_destra;
	}
	public void setCounter(int contatore) {
		this.contatore = contatore;
	}

	//metodo per gestire i movimenti dei personagi

	public Image walk(String nome , int frequenza ){

		String str ;
		ImageIcon ico ;
		Image img ;

		if(!this.in_movimento){

			if(this.verso_destra){ // se guarda a destra
				str = "/res/images/" + nome + "AD.png";
			}else str = "/res/images/" + nome + "AG.png"; //se non
		}else {
			this.contatore++;
			if(this.contatore / frequenza == 0){ //
				if(this.verso_destra){
					str = "/res/images/" + nome + "AD.png"; // mario fermo a destra
				}else str = "/res/images/" + nome + "AG.png";// mario fermo a sinistra
			}else{
				if(this.verso_destra){
					str = "/res/images/" + nome + "D.png"; // mario che camina verso destra
				}else str = "/res/images/" + nome + "G.png";// mario che camina verso sinistra
			}
			if(this.contatore == 2* frequenza )
				this.contatore = 0 ;
		}


		ico = new ImageIcon(getClass().getResource(str));
		img = ico.getImage();

		return img;
	}

	public void moving(){
		if(Start.scene.getxPos() >= 0){
			this.x = this.x - Start.scene.getMov();
		}
	}

	public boolean contactHorizontalElement(Element og){
		if(this.x + this.l < og.getX() || this.x + this.l > og.getX() + 5 ||
				this.y + this.h <= og.getY() || this.y >= og.getY() + og.getH() ){
			return false;
		}
		else if(this.x > og.getX() + og.getL() ||this.x + this.l < og.getX() + og.getL() -5 ||
				this.y + this.h <= og.getY() || this.y >= og.getY() + og.getH() ) {
			return false;
		}
		else
			return true;
	}

	protected boolean contactVerticalElement(Element og){
		if(this.x + this.l < og.getX() + 5 || this.x  > og.getX() + og.getL() - 5 ||
			this.y + this.h < og.getY() || this.y +this.h > og.getY() + 5 ){
			return false;
		}else if(this.x + this.l < og.getX() + 5 || this.x  > og.getX() + og.getL() - 5 ||
				this.y < og.getY() + og.getH() || this.y > og.getY()+ og.getH() +5 ) {
			return false;
		}
		else
			return true;
	}

	public boolean isNearElement(Element obj){

		if((this.x > obj.getX() - 10 && this.x < obj.getX() + obj.getL() + 10) ||
			(this.getX()+ this.l > obj.getX() - 10 && this.x + this.l < obj.getX() + obj.getL() + 10)) {
				return true;
		}else
			return false ;

	}


	protected boolean contactHorizontalCharacter(CharacterImpl pers){
		if(this.isMoovingToRight()){
			if(this.x + this.l < pers.getX() || this.x + this.l > pers.getX() + 5 ||
					this.y + this.h <= pers.getY() || this.y >= pers.getY() + pers.getH()){
				return false;
			}
			else if(this.x > pers.getX() + pers.getL() || this.x + this.l < pers.getX() + pers.getL() - 5 ||
					this.y + this.h <= pers.getY() || this.y >= pers.getY() +pers.getH()) {
				return false;
			}
			else{
				return true;
			}
		}

		return false;
	}

	public boolean contactVerticalCharacter(CharacterImpl pers){
		if(this.x + this.l < pers.getX() || this.x > pers.getX() + pers.getL() ||
				this.y + this.h < pers.getY() || this.y + this.h > pers.getY()){
			return false;
		}
		else{return true;}
	}

	public boolean isNearEnemy(Enemy enemy){
		if((this.x > enemy.getX() - 10 && this.x < enemy.getX() + enemy.getL() + 10)
		    	|| (this.x + this.l > enemy.getX() - 10 && this.x + this.l < enemy.getX() +enemy.getL() + 10)){
			return true;
			}
		else{
			return false;
		}
	}
}
