package GameComponent.Character;

import GameComponent.Object.Element;

import java.awt.Image;
import javax.swing.ImageIcon;
/**
 * Created by Alessio Addimando on 04/03/2017.
 */
public abstract class Enemy extends CharacterImpl implements Runnable {

    private Image image;
    private ImageIcon icon;

    private final int PAUSE = 15;
    private int index;

    public Enemy(int x, int y, final int width, final int heigt, final String pathImage) {
        super(x, y, width, heigt);
        super.setMoovingRight(true);
        super.setMooving(true);
        this.index = 1;
        this.icon = new ImageIcon(getClass().getResource(pathImage));
        this.image = icon.getImage();
    }

    private Image getImage() {
        return image;
    }

    public void moveCharacter() {
        if (super.isMoovingToRight()) {
            this.index = 1;
        } else {
            this.index = -1;
        }
        super.setX(super.getX() + this.index);
    }

    @Override
    public void run() {
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {

        }
        while (true) {
            if (this.isAlive()) {
                this.moveCharacter();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {

                }
            }
        }
    }

    public void contactWithComponent(Element component) {
        if (super.contactHorizontalElement(component) && this.isMooving()) {
            super.setMoovingRight(false);
            this.index = -1;
        }
    }

    public abstract Image afterDeath();

}
