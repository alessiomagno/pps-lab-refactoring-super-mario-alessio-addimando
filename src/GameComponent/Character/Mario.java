package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

import GameComponent.Object.Element;
import GameLogic.Start;
import GameComponent.Object.Piece;

public class Mario extends CharacterImpl {

	private ImageIcon Icopilar ;
	private Image imgpilar ;
	private boolean salto;
	private int compteurSaut ; //durata e intensit� del salto
	
	public Mario(int X, int Y) {
		super(X, Y, 28 , 50);
		Icopilar = new ImageIcon(getClass().getResource("/res/images/marioD.png"));
		this.imgpilar = this.Icopilar.getImage();
		this.salto = false;
		this.compteurSaut = 0;
	}

	//getters
	public boolean isSalto() {
		return salto;
	}

	//seters
	public void setSalto(boolean salto) {
		this.salto = salto;
	}

	public Image getImgpilar() {
		return imgpilar;
	}

	// metodi
	@Override
public Image walk(String nome , int frequenza ){
		
		String str ;
		ImageIcon ico ;
		Image img ; 
		
		if(!this.in_movimento || Start.scene.getxPos() <=0 || Start.scene.getxPos() > 4600){ //se non si muove o � completamente in fondo a sinistra
			
			if(this.verso_destra){ // se guarda a destra
				str = "/res/images/" + nome + "AD.png";
			}else str = "/res/images/" + nome + "AG.png"; //se non
		}else {
			this.contatore++;
			if(this.contatore / frequenza == 0){ // 
				if(this.verso_destra){
					str = "/res/images/" + nome + "AD.png"; // mario fermo a destra
				}else str = "/res/images/" + nome + "AG.png";// mario fermo a sinistra
			}else{
				if(this.verso_destra){
					str = "/res/images/" + nome + "D.png"; // mario che camina verso destra
				}else str = "/res/images/" + nome + "G.png";// mario che camina verso sinistra
			}
			if(this.contatore == 2* frequenza )
				this.contatore = 0 ;
		}
		
		//images del personnagio
		ico = new ImageIcon(getClass().getResource(str));
		img = ico.getImage();
		
		return img;
	
	}
	
	public Image Salto(){
		
		ImageIcon ico ;
		Image img;
		String str ;
		
		this.compteurSaut++;
		
		//salita
		if(this.compteurSaut <= 41){
			if(this.getY() > Start.scene.getHauteurPlafond())
				this.setY(this.getY() - 4);
			else this.compteurSaut = 42 ;
			if(this.isMoovingToRight())
				str = "/res/images/marioSD.png";
			else str = "/res/images/marioSG.png";
			
			//discesa 
		}else if (this.getY() + this.getH() < Start.scene.getySol()){
			this.setY(this.getY() + 1);
			if(this.isMoovingToRight())
				str = "/res/images/marioSD.png";
			else str = "/res/images/marioSG.png";
			
			// salto finito
		}else {
			if(this.isMoovingToRight())
				str = "/res/images/marioAD.png";
			else str = "/res/images/marioAG.png";
			this.salto = false;
			this.compteurSaut = 0 ;	
		}
		//reinitialisazione img mario
		ico = new ImageIcon(getClass().getResource(str));
		img = ico.getImage();
		return img ;
				
	}
	
	public void contactEnemy(Element obj){
		//contactEnemy orizontale
		if(super.contactHorizontalElement(obj) && this.isMoovingToRight() ||
				 (super.contactHorizontalElement(obj))&& !this.isMoovingToRight()){
			Start.scene.setMov(0);
			this.setMooving(false);
		}
		
		//contactEnemy con oggetti sotto
		if(super.contactHorizontalElement(obj) && this.salto) { // mario salta su un oggetto
			Start.scene.setySol(obj.getY());
		}else if (!super.contactHorizontalElement(obj)){ // mario cade sul pavimento
			Start.scene.setySol(293);  // 293 che � il valore iniziale
			if(!this.salto){this.setY(243); // altezza iniziale di mario
		}
		
		// contactEnemy con un oggetto sopra
		if(contactVerticalElement(obj)){
			Start.scene.setHauteurPlafond(obj.getY() + obj.getH());
		}else if (!super.contactVerticalElement(obj) && !this.salto){
			Start.scene.setHauteurPlafond(0);
		}	
		}
	}
	
	public boolean contactPiece(Piece piece){
		// si controla avanti indietro , a destra e a sinistra
		if(this.contactHorizontalElement(piece)|| this.contactVerticalElement(piece) || this.contactHorizontalElement(piece)
				||this.contactVerticalElement(piece)){
			return true;
		}else return false;
	}
	
	public void contactEnemy(Enemy enemy){
		if((super.contactHorizontalCharacter(enemy)) || (super.contactHorizontalCharacter(enemy))){
			if(enemy.alive == true){
				this.setMooving(false);
		    	this.setAlive(false);
			}
			else this.alive = true;
		}else if(super.contactVerticalCharacter(enemy)){
			enemy.setMooving(false);
			enemy.setAlive(false);
		}
	}
}
