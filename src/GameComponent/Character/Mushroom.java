package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Mushroom extends Enemy implements Runnable{

	private Image imgFunghi;
	private ImageIcon icoFunghi;
	

	public Mushroom(final int x, final int y) {
			super(x, y,27 , 30, "/res/images/funghiAD.png");

			Thread mushroomThread = new Thread(this);
			mushroomThread.start();
		}

		public Image afterDeath(){
			String pathImage;
			ImageIcon icon;
			Image image;
			if(this.isMoovingToRight()){
				pathImage = "/imagine/funghiED.png";
			} else {
				pathImage = "/imagine/funghiEG.png";
			}
			icon = new ImageIcon(getClass().getResource(pathImage));
			image = icon.getImage();
			return image;
		}

	}
