package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Turtle extends Enemy implements Runnable {
	
	private Image imgTurtle;
	private ImageIcon icoTurtle;


	public Turtle(final int x, final int y) {
		super(x, y, 43, 50, "/res/images/turtleAD.png");

		Thread chronoTurtle = new Thread(this);
		chronoTurtle.start();
	}


	public Image afterDeath(){
				String str = "/res/images/turtleF.png";
				ImageIcon ico;
				Image img;		
		        ico = new ImageIcon(getClass().getResource(str));
		        img = ico.getImage();
				return img;
		}


}
