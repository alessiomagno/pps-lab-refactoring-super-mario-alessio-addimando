package GameComponent.Object;

import javax.swing.ImageIcon;

public class Block extends Element {
	
	public Block(int X, int Y) {
		
		super(X, Y, 30 ,30);
		super.icoObj = new ImageIcon(getClass().getResource("/res/images/Blocco.png"));
		super.imgObj = super.icoObj.getImage();
	}

	
}
