package GameComponent.Object;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Piece extends Element implements Runnable{

	private int contatore;
	private final int PAUSE = 10;

	public Piece(int X, int Y) {
		super(X, Y, 30 , 30);
		super.icoObj = new ImageIcon(this.getClass().getResource("/res/images/piece1.png"));
		super.imgObj = this.icoObj.getImage();
	}
	
	public Image muoviti(){
		
		ImageIcon ico ;
		Image img;
		String str ;
		this.contatore++;
		if(this.contatore / 100 == 0){
			str= "/res/images/piece1.png";
		}else str = "/res/images/piec.png";
		if(this.contatore == 200 ) {this.contatore = 0;}
		ico = new ImageIcon(getClass().getResource(str));
		img =ico.getImage();
		return img;
	}

	@Override
	public void run() {
		
		try{Thread.sleep(10);}
	catch(InterruptedException e){}
			
		while(true){
			this.muoviti();
			try{Thread.sleep(PAUSE);}
			catch(InterruptedException e){}
		}
		
	}

}
