 package GameComponent.Object;

import javax.swing.ImageIcon;

public class Tube extends Element {

	public Tube(int X, int Y) {
		
		super(X, Y, 43 ,65);
		super.icoObj = new ImageIcon(getClass().getResource("/res/images/tunello.png"));
		super.imgObj = super.icoObj.getImage();
	}

}
