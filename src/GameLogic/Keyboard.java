package GameLogic;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

	@Override
	public void keyPressed(KeyEvent e) {
		
		if(Start.scene.mario.isAlive()){
			if(e.getKeyCode() == KeyEvent.VK_RIGHT){
			
				// per non fare muovere il castello e start 
				if(Start.scene.getxPos() == -1){
					Start.scene.setxPos(0);
					Start.scene.setX1(-50);
					Start.scene.setX2(750);
				}
				Start.scene.mario.setMooving(true);
				Start.scene.mario.setMoovingRight(true);
				Start.scene.setMov(1); // si muove verso sinistra
			}else if(e.getKeyCode() == KeyEvent.VK_LEFT){
			
				if(Start.scene.getxPos() == 4601){
					Start.scene.setxPos(4600);
					Start.scene.setX1(-50);
					Start.scene.setX2(750);
				}
			
				Start.scene.mario.setMooving(true);
				Start.scene.mario.setMoovingRight(false);
				Start.scene.setMov(-1); // si muove verso destra
			}
			// salto
			if(e.getKeyCode() == KeyEvent.VK_UP){
				Start.scene.mario.setSalto(true);
				Audio.playSound("/res/audio/jump.wav");
			}
			
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Start.scene.mario.setMooving(false);
		Start.scene.setMov(0);
	}

	@Override
	public void keyTyped(KeyEvent e) {}

}
