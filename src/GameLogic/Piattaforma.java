package GameLogic;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import GameComponent.Character.Mushroom;
import GameComponent.Character.Mario;
import GameComponent.Object.Block;
import GameComponent.Object.Element;
import GameComponent.Object.Piece;
import GameComponent.Object.Tube;
import GameComponent.Character.Turtle;

@SuppressWarnings("serial")
public class Piattaforma extends JPanel{
	
	private ImageIcon backgroundIco ;
	private Image imgbackground ;
	private Image imgbackground2 ;
	
	private ImageIcon casteloico ;
	private Image castelo ;
	private ImageIcon startico ;
	private Image start ;
	
	//dimension con estremita a sinistra del background
	private int x1 ; //background 1
	private int x2 ; // altri background 
	private int mov ; // indice per il movimento che verra incrementato o decrementato
	private int xPos;  // variabile per riperire gli elementi del gioco sul l'asse X
	private int ySol ; // altezza del pavimento
	private int hauteurPlafond ;
	
	public Mario mario;
	
	//mushroomInGame del gioco
	public Mushroom mushroomInGame;
	
	//Turtle in the GameLogic
	public Turtle Turtle;
	
	
	// tunello nel gioco
	public Tube tube1;
	public Tube tube2;
	public Tube tube3;
	public Tube tube4;
	public Tube tube5;
	public Tube tube6;
	public Tube tube7;
	public Tube tube8;
	
	// bloco del gioco
	public Block block1;
	public Block block2;
	public Block block3;
	public Block block4;
	public Block block5;
	public Block block6;
	public Block block7;
	public Block block8;
	public Block block9;
	public Block block10;
	public Block block11;
	public Block block12;
	
	public Piece piece1;
	public Piece piece2;
	public Piece piece3;
	public Piece piece4;
	public Piece piece5;
	public Piece piece6;
	public Piece piece7;
	public Piece piece8;
	public Piece piece9;
	public Piece piece10;
	
	// bandiera e castello di fine
	private ImageIcon icoBandiera ;
	private Image imgBandiera ;	
	private ImageIcon icoCastelloF ;
	private Image imgCastelloF ;

	private ArrayList<Element> tabobj ; //tabella per salvare tutti gli oggeti
	private ArrayList<Piece> tabPieces ; // table with all the piece
	
	//costrutore
	public Piattaforma(){
		
		super();
		this.x1 = -50 ;
		// -50 + width = 750
		this.x2 = 750 ;
		this.mov = 0;
		this.xPos = -1 ;
		this.ySol = 293;
		this.hauteurPlafond = 0;
		backgroundIco = new ImageIcon(getClass().getResource("/res/images/background.png"));
		this.imgbackground = this.backgroundIco.getImage();
		this.imgbackground2 = this.backgroundIco.getImage();
		casteloico = new ImageIcon(getClass().getResource("/res/images/castelloIni.png"));
		this.castelo = this.casteloico.getImage();
		startico = new ImageIcon(getClass().getResource("/res/images/start.png"));
		this.start = this.startico.getImage();
		mario = new Mario(300, 245);
		mushroomInGame = new Mushroom(800, 263);
		Turtle = new Turtle(950, 243);
		// position of all the Element
		
		tube1 = new Tube(600, 230);
		tube2 = new Tube(1000, 230);
		tube3 = new Tube(1600, 230);
		tube4 = new Tube(1900, 230);
		tube5 = new Tube(2500, 230);
		tube6 = new Tube(3000, 230);
		tube7 = new Tube(3800, 230);
		tube8 = new Tube(4500, 230);
		
		block1 = new Block(400, 180);
		block2 = new Block(1200, 180);
		block3 = new Block(1270, 170);
		block4 = new Block(1340, 160);
		block5 = new Block(2000, 180);
		block6 = new Block(2600, 160);
		block7 = new Block(2650, 180);
		block8 = new Block(3500, 160);
		block9 = new Block(3550, 140);
		block10 = new Block(4000, 170);
		block11 = new Block(4200, 200);
		block12 = new Block(4300, 210);
		
		piece1 = new Piece(402, 145);
		piece2 = new Piece(1202, 140);
		piece3 = new Piece(1272, 95);
		piece4 = new Piece(1342, 40);
		piece5 = new Piece(1650, 145);
		piece6 = new Piece(2650, 145);
		piece7 = new Piece(3000, 135);
		piece8 = new Piece(3400, 125);
		piece9 = new Piece(4200, 145);
		piece10 = new Piece(4600, 40);
		
		
		this.icoCastelloF = new ImageIcon(getClass().getResource("/res/images/castelloF.png"));
		this.imgCastelloF = icoCastelloF.getImage();
		
		this.icoBandiera = new ImageIcon(getClass().getResource("/res/images/bandiera.png"));
		this.imgBandiera = icoBandiera.getImage();
		
		tabobj = new ArrayList<Element>();
		
		this.tabobj.add(tube1);
		this.tabobj.add(tube2);
		this.tabobj.add(tube3);
		this.tabobj.add(tube4);
		this.tabobj.add(tube5);
		this.tabobj.add(tube6);
		this.tabobj.add(tube7);
		this.tabobj.add(tube8);
		
		this.tabobj.add(block1);
		this.tabobj.add(block2);
		this.tabobj.add(block3);
		this.tabobj.add(block4);
		this.tabobj.add(block5);
		this.tabobj.add(block6);
		this.tabobj.add(block7);
		this.tabobj.add(block8);
		this.tabobj.add(block9);
		this.tabobj.add(block10);
		this.tabobj.add(block11);
		this.tabobj.add(block12);
		
		tabPieces = new ArrayList<Piece>();			
		this.tabPieces.add(this.piece1);
		this.tabPieces.add(this.piece2);
		this.tabPieces.add(this.piece3);
		this.tabPieces.add(this.piece4);
		this.tabPieces.add(this.piece5);
		this.tabPieces.add(this.piece6);
		this.tabPieces.add(this.piece7);
		this.tabPieces.add(this.piece8);
		this.tabPieces.add(this.piece9);
		this.tabPieces.add(this.piece10);
	
		//schermo listener
		this.setFocusable(true);
		this.requestFocusInWindow();
		
		//collegamento con la classe keyboard
		this.addKeyListener(new Keyboard());
	}
	
	//getters

	public int getySol() {
		return ySol;
	}

	public int getHauteurPlafond() {
		return hauteurPlafond;
	}

	public int getMov() {
		return mov;
	}
	
	public int getxPos() {
		return xPos;
	}
		
	//setters
	public void setX2(int x2) {
		this.x2 = x2;
	}

	public void setySol(int ySol) {
		this.ySol = ySol;
	}

	public void setHauteurPlafond(int hauteurPlafond) {
		this.hauteurPlafond = hauteurPlafond;
	}

	public void setxPos(int xPos) {
		this.xPos = xPos;
	}

	public void setMov(int mov) {
		this.mov = mov;
	}
	
	public void setX1(int x) {
		this.x1 = x;
	}
	
	// metodo per gestire la permanenza del fondo (si muove il background )	
	public void movimento_di_fondo(){
		/* per bloccare la posizione il ritorno verso la sinistra con il castello poi si aggiorna il valore
		 di xpos dentro la classe keyboard affinche non sia mai negativo */
		if(this.xPos >= 0 && this.xPos <= 4600){
			this.xPos = this.xPos + this.mov ;
			this.x1 = this.x1 - this.mov ;  // si muove la schermato per dare l'impressione che mario camina
			this.x2 = this.x2 - this.mov ;  // si muove la schermato per dare l'impressione che mario camina
		}
		 
		//condizioni per aggiornamento del background a l'infinito  	
		if(this.x1 == -800){this.x1 = 800;}		// dove il background 1 finisce si mette il 2 (a destra )	
		else if (this.x2 == -800){this.x2=800;}	// dove il background 2 finisce si mette il 1 (a destra )	
		else if (this.x1 == 800){this.x1=-800;}	// dove il background 1 finisce si mette il 2 (a sinistra )			
		else if (this.x2 == 800){this.x2= -800;}// dove il background 2 finisce si mette il 1 (a sinistra )	
	}

	// disegno dei GameComponent.Character.Character.Object
	public void paintComponent(Graphics g){		 
		super.paintComponent(g);
		Graphics g2 = (Graphics2D)g; 		
		//detezione contactEnemy con l'oggetto piu isNearCharacter di lui
		for (int i = 0 ; i<tabobj.size() ; i++){
			//contactEnemy di mario con gli oggetti
			if(this.mario.isNearElement(this.tabobj.get(i)))
				this.mario.contactEnemy(this.tabobj.get(i));
			//contactEnemy di mario con gli oggetti
			if(this.mushroomInGame.isNearElement(this.tabobj.get(i)))
				this.mushroomInGame.contactWithComponent(this.tabobj.get(i));
			//contactEnemy Turtle
			if(this.Turtle.isNearElement(this.tabobj.get(i)))
				this.Turtle.contactWithComponent(this.tabobj.get(i));
		}
		
		//detection with the piece 
		for(int i=0 ; i <tabPieces.size() ; i++){
			if(this.mario.contactPiece(this.tabPieces.get(i))){
				Audio.playSound("/res/audio/money.wav");
				this.tabPieces.remove(i);
			}
		}
		
		//contactEnemy fra Turtle e mushroomInGame
		if(this.mushroomInGame.isNearEnemy(Turtle)){this.mushroomInGame.contactVerticalCharacter(Turtle);}
		if(this.Turtle.isNearEnemy(mushroomInGame)){this.Turtle.contactVerticalCharacter(mushroomInGame);}
		if(this.mario.isNearEnemy(mushroomInGame)){this.mario.contactVerticalCharacter(mushroomInGame);}
		if(this.mario.isNearEnemy(Turtle)){this.mario.contactEnemy(Turtle);}
		
		// spostamento oggetti fissi
		this.movimento_di_fondo(); 
		if(this.xPos >= 0 && this.xPos <= 4600){
			for (int i = 0 ; i<tabobj.size() ; i++){
				tabobj.get(i).spostamenti();
			}
			
			for(int i=0 ; i <tabPieces.size() ; i++){
				this.tabPieces.get(i).spostamenti();
			}
			
			this.mushroomInGame.moving();
			this.Turtle.moving();
		}
		
		g2.drawImage(this.imgbackground, this.x1, 0, null);
		g2.drawImage(this.imgbackground2, this.x2, 0, null); // disegno images di fondo2
		g2.drawImage(this.castelo, 10 - this.xPos, 95, null);
		g2.drawImage(this.start, 220-this.xPos, 234, null);
		
		// disegno di tutti gli oggetti	
			for (int i = 0 ; i<tabobj.size() ; i++){
				g2.drawImage(this.tabobj.get(i).getImgObj(),this.tabobj.get(i).getX() ,
						this.tabobj.get(i).getY(), null);
			}
		
		//design of piece's image 
		for(int i=0 ; i <tabPieces.size() ; i++){
			g2.drawImage(this.tabPieces.get(i).muoviti(), this.tabPieces.get(i).getX(),
					this.tabPieces.get(i).getY(), null);
		}
		// disegno castello fine e bandiera
		g2.drawImage(this.imgBandiera, 4650 -this.xPos, 115, null);
		g2.drawImage(this.imgCastelloF, 4850 - this.xPos, 145, null);
		
		//disegno di mario
		if(this.mario.isSalto())
			g2.drawImage(this.mario.Salto(), this.mario.getX(), this.mario.getY(), null);
		else g2.drawImage(this.mario.walk("mario", 25), this.mario.getX(), this.mario.getY(), null);
			
		//disegno del fungho
		if(this.mushroomInGame.isAlive())
			g2.drawImage(this.mushroomInGame.walk("mushroomInGame", 45), this.mushroomInGame.getX(), this.mushroomInGame.getY(), null);
		else
			g2.drawImage(this.mushroomInGame.afterDeath(), this.mushroomInGame.getX(), this.mushroomInGame.getY() +20 , null);
		
		//disegno Turtle
		if(this.Turtle.isAlive())
			g2.drawImage(this.Turtle.walk("Turtle", 45), this.Turtle.getX(), this.Turtle.getY(), null);
		else g2.drawImage(this.Turtle.afterDeath(), this.Turtle.getX(), this.Turtle.getY() + 30, null);
	}
	
}
