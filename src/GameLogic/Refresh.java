package GameLogic;

public class Refresh implements Runnable {

	private final int PAUSE = 3 ; // tempo di attesa per aggiornare lo schermo
	
	public void run() {
		
		// si ridesegna lo schermo ogni pausa millisecondi
		while (true){
			Start.scene.repaint();
			//Start.test.paintAll(Start.test.getGraphics());
			try {
				Thread.sleep(PAUSE);
			} catch (InterruptedException e) {
				//e.printStackTrace();
			}
		}
	}
	
} 
